# Pluie

Pluie est une unique page web qui affiche 
 - les conditions météorologiques actuelles pour la ville de la première colonne
 - l'indice de qualité de l'air pour la ville de chaque colonne
 - le lever et coucher du soleil
 - les prévisions de pluie dans l'heure de Météo France] pour chaque ville

Il faut passer le nom d'une ou plusieurs villes en paramètres. Les données OpenWeatherMap et l'indice de qualité de l'air sont affichés uniquement pour la première ville. Les prévision de pluie sont affichée pour chaque ville.

Les données sont téléchargées **par le navigateur** depuis les sources suivantes, le serveur ne télécharge rien :
 - [OpenWeatherMap](http://openweathermap.org) : conditions météo courantes, lever / coucher du soleil
 - [World Air Quality Index project](http://aqicn.org) : indice de qualité de l'air
 - du service [« va-t-il pleuvoir dans l'heure »](http://www.meteofrance.com/previsions-meteo-france/previsions-pluie) de Météo France

Le service de Météo France ne couvre pas la totalité du territoire. Ceci peut expliquer que vous ne trouviez pas une commune.

Les données sont mises en forme via des modèles [handlebars](http://handlebarsjs.com/) et le framework [Bootstrap](http://getbootstrap.com/). Le traitement est réalisé avec [jQuery](https://jquery.com/). Le projet [Weather Icons](http://erikflowers.github.io/weather-icons/) fournit les icônes.

Les CSS et fichiers Javascript sont téléchargés depuis des [CDN](https://fr.wikipedia.org/wiki/Content_delivery_network).


En espérant que ce service puisse être utile aux [vélotafeurs](https://droitauvelo.org/+Un-velotaffeur+) !

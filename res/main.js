$(function(){
	"use strict";

	function removeCity(id){
		var cities;
		var cityIndex = id.replace('city_', '');


		cities = getStorageItem('cities');
		cities.splice( cityIndex, 1 );
		localStorage.setItem("cities", JSON.stringify(cities) );

		$( document ).trigger('show');
	}

	function addCity(){
		$( "#addCity" ).replaceWith('<th class="ui-widget"><input id="cityToAdd" class="ui-autocomplete-input" type="search" autocomplete="on" inputmode="latin" placeholder="Ajouter une commune"></th>');
		$( "#cityToAdd" ).autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "//ws.meteofrance.com/ws/getLieux/pluie/" + request.term + ".json",
					dataType: "jsonp",
					success: function(data){
						response($.map(data.result, function(item) {
							return {
								label: item.nom + ' (' + item.codePostal +')',
								value: item.nom,
								json: item
							};
						}));
					}
				});
			},
			minLength: 3,
			select: function(event, ui) {
				addCityToStorage(ui.item.json);
			}
		});
	}

	function getData(url) {
		return $.ajax({
			url: url,
			dataType: "jsonp"
		});
	}

	function showCurrentWeather (data) {
		var source = document.getElementById("currentWeatherTemplate").innerHTML;
		var template = Handlebars.compile(source);
		var output = template(data);
		var content = document.getElementById("currentWeather");
		content.innerHTML = output;

		source = document.getElementById("sunTemplate").innerHTML;
		template = Handlebars.compile(source);
		output = template(data);
		content = document.getElementById("sun");
		content.innerHTML = output;
	}

	function showCurrentAqi (data) {
		var source = document.getElementById("aqiTemplate").innerHTML;
		var template = Handlebars.compile(source);
		var output = template(data);
		var content = document.getElementById("aqi");
		content.innerHTML = output;

		source = document.getElementById("aqiDataSourceTemplate").innerHTML;
		template = Handlebars.compile(source);
		output = template(data);
		content = document.getElementById("aqiDataSource");
		content.innerHTML = output;
	}


	function showRain (data) {
		// sets variable source to the rainTemplate id in index.html
		var source = document.getElementById("rainTemplate").innerHTML;

		// Handlebars compiles the above source into a template
		var template = Handlebars.compile(source);

		// data is passed to above template
		var output = template(data);

		// remove rain table if exists
		removeTable();

		// HTML element with id "templateContainer" is set to the output above
		var templateContainer = document.getElementById("templateContainer");
		templateContainer.innerHTML = templateContainer.innerHTML + output;
	}

	function faviconChange (icon) {
		var icons = {
			sun: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAERgAABEYBZgGbmQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAABYdEVYdENvcHlyaWdodABDQzAgUHVibGljIERvbWFpbiBEZWRpY2F0aW9uIGh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL3B1YmxpY2RvbWFpbi96ZXJvLzEuMC/G4735AAAEBElEQVR4nO2bTYiVVRyHn7cmU5tZhCRJy6jJQmwZSEpQRC0iN2VBVAsL+rKMyPZ9LCwSgqDogwQToU0WOWUuLGtjtJssGbNCKnCTaGX48bS45+bl4tz7zj3/954J7g8Od7j3nOf8f7/7znDec96BglKeV3aXrOGCkpMDVwFXlyygdADFNQqgdAGlNQqgdAGlNQqgTiflIqVqupgSqnsF7AWmlEVNFhMpZZ1yW79+dQPYBtwCfKgszKpsCFI2AO8Dd4ZClbPKp1FXgrJD+TmC1cF8ONX5WfgVqzypmELIvhKiA2jUfMckYSFEBjAU8x2TPZVCmMoJISoA5aFkfq9ySS6v7qTZIUQEUMR8x+QbUwi7Bgkhjd+aMX858x1FZIWQMe/6CPMhqzvlaeBlYApYW1Wc7NH3emAVMAlcnt4+AcwA+4F9VcXffeZbD7wB7ANurypOZJvIlbIpXQk7lQVdn00ozygzqU+vdlx5S7lmlnna3/wXyvhw3NWU8lwycX/He/cov9cw3t1OK691Xt7Kvcn8HmVxGZd9pKxWFisLlLcHMN7dvlOuTOz7lO3z1nxbtu4edwaYb7dflcnSvmpLeTPQfLv9qCwp7a2vlLsaMN9uH5T211PKuPJbgwGo3Fra56zy3PK4yfZ1aZ+zSjkwhABUVkTVHLYpqlwL51+8NKC1UaDIXeFVgax+ujEKFBnA8kDW0OYaU5YDV2QwDlYVvwBLg2qqo6UAyoPADRmcn8ZobXlflgH5ElgNjGUw5qoL0+sa4I4MzoEx4CZgWQbkUHo9lsGYq44BVBUP5ILGqoppYDoXBBwOYNTVof5d6inyj+D+QFY/fRsFigzgK+B4IK+XPokChQWQtsF2RPF66CitrbcQRR+PvwKcCWZ2a0tV8U/Dcwwu5fUG7wEO+z/YDZpQDjZg/rSyprS/WlImlaPBATxa2ldfKQuVlennFcqRoG/+scRc4nx9UsXWTvAe5ZTpdFZZZuu0dlDzR5SbO/gnla3631J4fqjD/Fnlka7PKuVuZXoOxv9QXlAmulib0+fvzZsQlEXK7mT+8R79KltnBq8q3yh/dhg+ZevUaJutvf9ZT3yUF9OY7TrUG7DzFlPLfI/x48qlA4wrH0Ku+cRYp3w+4NjOEIb766BcrHyczD+RwdmsmDH+paGHEGU+sbICSIzhhdBhXuXZAF52AInTfAi2Tns/ijKfmCEBJFZzIXSZ3xTIDQsg8dohvKtBd7q2jrp3JfDGEOg5dnQAVVpnqLxjxLJZuU45Y+s5oFBFB5CYlbIlhbAyCtrIPXgTASRu5SzPGHWr1iqqqvgrr6ThqqoQ+L5O39F/jJQuoLRGAZQuoLRGAZQuoLRKBzAD/FCygH8BA5UwcibmaRQAAAAASUVORK5CYII=",
			rain: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAcdEVYdFRpdGxlAHNpbXBsZSB3ZWF0aGVyIHN5bWJvbHOcuSrFAAAAEHRFWHRBdXRob3IAQW5vbnltb3VzIQR0fwAAACF0RVh0Q3JlYXRpb24gVGltZQAyMDA4LTAyLTA4VDIzOjU0OjA2Zjt2vwAAAFV0RVh0U291cmNlAGh0dHBzOi8vb3BlbmNsaXBhcnQub3JnL2RldGFpbC8xNDcwOC9zaW1wbGUtd2VhdGhlci1zeW1ib2xzLWJ5LWFub255bW91cy0xNDcwOIJBHQUAAABYdEVYdENvcHlyaWdodABDQzAgUHVibGljIERvbWFpbiBEZWRpY2F0aW9uIGh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL3B1YmxpY2RvbWFpbi96ZXJvLzEuMC/G4735AAAGfElEQVR42u1ae0xbZRTHVzQ+o/EZ348oPpYYjTGaGPWP/WP0n8VHjFlUhFLYCPNBtuncmNk0OjXLNii3PIQNeWULGQ+RiRsThDGcyBiMycI22dp7b0tflJZS2uM5bSG3l3tbXi29TX/JCfT23vb7nZ739yUlJZBAAgkkEFtQMXCVWsM+pWL0r6YXcKvSC/jXMvINz6o1xrtzc+HKuCSdXqB/QsVwW5BwF8oECsiIDaUWRU0KUTzxNK3hZSTzewjCocSl1vCFqLiHFUf8wxL9bWjaVQskLhY3ynfkOgoxd+4FFE6KzJpCHn6sN8OBzjFoPeWAriEndAw6oalnHCrbbbC1ZjSUIo6lFrIPxjh59g1cqEO8+C3Vo/AnEnW4vBAOJrsHGk/Y4ZNSg5QSOHSrR2PU37kXxeSzS3g42u8Ab3jeszAx6YXaY2OgZoKVgDHhfGrh6D3LwxLgMlzEShXD7sa/LRTVcUH7VQy/TWz2X1YagbdMwWIxMOKCT2dbw99RT5kB3z4xl8C1GcnbnB5YKlwwuH3WFGwJ7Lqokcd0lEJpaS7ks4t54K1TsNQYuOgCdfB3WVWM4a5okH8bv8wjRXRjuREytcHX/xhwQKRQdsQa9F1YUX4RUfKZeZfuJU0LvzQXI/qp/1zgCQQ2l9sLbUha02yBlt5xiCTIrSiVCtbTt2iSH/x07hqKquhTK1SMMTlr1+iNAr/fJyT//UGTLzovJzS/WoKsIE2rf3JehDF6Xq7W6F/BhzUYwXsDlZbYlymddQjfo0hsc3hgudH1r1PkBnzKPFIY+w4+dHYhJWlzzzjEAi6Z3KK1sV+H5f5R0cgtePOhhdbjVIxYxz0xoQCyQtH68sIEMv5ONPUhKWJUbu5uNENNxxg0/GWHuuN2KGu1wg915qDoTn4XKzDYpsQ8doQcPuANbWLiOxvM0C+I5FJwYt3ec24COs84fZE+VjCI9YCoNO7FSnQDzRzkCpiZm7OKeDiOHZiS0YCNkoyrerEFr1cxuvuE5evJGT9G6bvgAqWjss0WLmaZaMQ2rQDv9Bvk6/GAS6NuyNnrb47WifoDYduMlnBrkvDiQQxw8YIpTEjjE/64RDMGqkQ/m90x7glSQGGLFeIZ1HqLOkZTEg0PZsZRWt7XVsYzxPEBFcB+LryQU2aAYW4ybhVAs8YgBazey16H/wwKL2ZggVOBmopEz76cGMdYsKE8KA44A8UQu0Kt4XipaLlt/yhUtdug7bRjJqgoEed5t4+LqEeoFlSExmS8cDrkGKvK6IuuSiiEPsZgt6nSCDvrzbCpwijFx4m1wGOz+n60hFx80yynhKUYYkYSZrtHPBaTkimaYsn2BynFhhtQEel4YxPK5PSDlEe9Me4F9gkPZDAhyQ/TrGM+k95m4QfQ7syEOza1QOMw8TSIBjf+Tpetxh/1vXlvnwUGnkEfSmXm4b7YCYpk9tSqZ88ue70zNf9iNjpQc6Vy+3ikcdq7M0QxZZLadSY3tPY7fEFOLWPySzYR9s8Iua/Czf3X7zMAc8gCdd126D7rhBGj2zczWJRPo1lTCqOZA22D5TVZpHaBxDJGMSwCI3D+EYywNcIOci6ytoiHjT8b4dtak+8XK2i2gBYVVXrECuVHbVBy2Op7TdZE739zwAQbUJlUms9zNDeJxPNTC7k7orAXwK8XV5DLKP20HhrtRX0vNDNP93g6w6lwEeXCxiqCQjtQg3QyBH/t1Wv26O6PqS1wqiHStPrnVAz/Pi5yOwVQlF9w0f+g6KgFDVWgBN6/iGmr2z++Yovx9VZ8/W6aln+airW4ORhFysrIt9xMDVnibFz8nvkz3YSusHae2eb6qGxrR+kI3AgGx5y5K8zwDD5zilxDscTpaEqgcJryb0wYk+e4L5kdOCTZoljylJaQQLsgsp8J32+wtwcyxfSgIluhx135NyVmCTvCdJsrUfRBI7l87iHFmTz6eYFUbs/I518K4Sa5EkdrTirrZ/cfi6uQKWwMb9XAFbMDne5aJP+bdAfHb1dYmmNTZc8MYBUoY/blcs/g5z2vML/nhkOUtqvE95NLhLhfR623YshT9A5BhpWq2wMnSEGlkXwmT3G1/HSul5BMaZfhK+V6+Zg95BzGBaT8uVzOlDFVZskoYLMicz+ZORYum5BAp+/YHMOpKDPI3Z+1a+hqvL9R+MtTuxvqmbiEWqN/AGPI68syxUkggQQSSMCP/wF9g9n3y2YWLgAAAABJRU5ErkJggg=="
		};

		$('#favicon').remove();
		$('head').append('<link id="favicon" href="' + icons[icon] + '" rel="shortcut icon">');
	}

	function faviconChangeIfNeeded(element, index, array) {
		if ( element > 1 ) {
			faviconChange('rain');
		}
	}

	function faviconChangeIfJsonNeeds (rain){
		var interval;

		for ( interval in rain ) {
			rain[interval].forEach( faviconChangeIfNeeded );
		}
	}

	function addResultToStorage(data){
        addCityToStorage(data.result[0]);
		//for (var i=0 ; i < data.result.length ; i++) {
		//	addCityToStorage(data.result[i]);
		//}
	}

	function addCityToStorage(city){

		var cities =[];
		var citiesFromStorage = localStorage.getItem("cities");

		if ( citiesFromStorage ) {
			citiesFromStorage = JSON.parse(citiesFromStorage);
			citiesFromStorage.push(city);
			cities = citiesFromStorage;
		} else {
			cities[0] = city;
		}

		localStorage.setItem("cities", JSON.stringify(cities) );
		$( document ).trigger('show');
	}

	function getCityDetails(city) {
		var url;
		url = "http://ws.meteofrance.com/ws/getLieux/pluie/"+ city + ".json";
		getData(url).done(addResultToStorage);
	}

	function getStorageItem(key){
		var value = localStorage.getItem(key);
		if ( value ) {
			value = JSON.parse(value);
		}

		return value;
	}

	function showContent (){

		var cities = [];
		var weatherUrl = '';
		var airQualityUrl = '';
		cities = getStorageItem("cities");

		faviconChange('sun');

		// remove rain table if exists
		removeTable();

		// get and show data from OpenWeatherMap
		weatherUrl = "https://api.openweathermap.org/data/2.5/weather?APPID=" + token.openweather + "&units=metric&lang=fr&lat=" + cities[0].latitude + "&lon=" + cities[0].longitude;
		airQualityUrl = "https://api.waqi.info/feed/geo:" + cities[0].latitude + ";" + cities[0].longitude + "/?token=" + token.aqicn;
		getData(weatherUrl).done(showCurrentWeather);
		getData(airQualityUrl).done(showCurrentAqi);

		// get and show pluie data
		getRain(cities);

		// for statstics only
		// stats(cities);

	}

	function removeTable(){
		var rainTable = document.getElementById("rainTable");
		if ( rainTable ){
			rainTable.remove();
		}
	}

	function getRain(cities) {
		var deferredsRain = [];
		var jsonData = {};
		var url = '';

		jsonData.citiesDetails = [];
		jsonData.rain = {};

		cities.forEach(
			function(element, index, array){
				url= "//ws.meteofrance.com/ws/getPluie/"+ element.indicatif + "0.json";
				deferredsRain.push(getData(url));
			}
		);

		$.when.apply($, deferredsRain).done(
			function(data) {
				if ( Array.isArray(arguments[0]) ) {
					for (var i = 0; i < arguments.length; i++) {
						jsonData.rain = jsonRainToJsonData(arguments[i][0].result.intervalles, jsonData.rain);
					}
				} else {
					jsonData.rain = jsonRainToJsonData(arguments[0].result.intervalles, jsonData.rain);
				}
			}
		).then(
			function() {
				jsonData.citiesDetails = cities;
				faviconChangeIfJsonNeeds(jsonData.rain);
				showRain(jsonData);
				addCity();
			}
		);

	}

	function jsonRainToJsonData(intervals, rain) {

		for ( var i = 0; i < intervals.length; i++) {
			if ( typeof rain[ intervals[i].date ] === 'undefined') {
				rain[ intervals[i].date ] = [];
			}
			rain[ intervals[i].date ].push(intervals[i].value);
		}

		return(rain);
	}


	function stats(cities) {
		var url = '/s?';

		for (var i=0;i<cities.length;i++){
			url = url + 'c=' + cities[i].nom + '&';
		}

		$.ajax({
			url: url,
			dataType: "html"
		});
	}

	function initEvents(){
		$( document ).on('show', function() {
			showContent();
		});

		$( '#templateContainer' ).on('click', '#rainTable thead tr th.hand', function(event) {
			removeCity(event.currentTarget.id);
		});
	}

	function appFirstTime() {
		//var defaultCities    = ['Vern-sur-Seiche'];
		var defaultCities    = ['Anglet', 'Saint-Martin-de-Hinx'];
		var cities = [];
		var i;

		var event = new Event('show');

        var queryString = (window.location.href.split('?')[1] != null ? window.location.href.split('?')[1] : "");
        queryString = queryString.split('#')[0];
        var arr = queryString.split('&');
        if (arr.length > 1) {
            localStorage.setItem("cities", JSON.stringify(cities) );
            for (i = 0; i < arr.length; i++) {
                var a = arr[i].split('=');
                var paramName = a[0];
                var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
                if (paramName == 'c') {getCityDetails(paramValue);}
            }
        }else {
            cities = getStorageItem("cities");
            if ( !cities || cities.length === 0 ) {
                for (i = 0; i < defaultCities.length; i++) {
                getCityDetails(defaultCities[i]);
                }
            } else {
                showContent();
            }
        }
	}

	function main () {
		var refresh    = 300000;

		document.getElementById("header");

		initEvents();
		appFirstTime();

		setInterval(
			function(){
				showContent();
			}, refresh);
	}

	main();
});

// vim:set tabstop=4 shiftwidth=4 softtabstop=4 expandtab

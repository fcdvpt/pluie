var epocMsecToDate = function(milliseconds) {
	var d = new Date(parseInt(milliseconds));
	// ne fonctionne pas avec tous les navigateurs
	//time = d.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});
	var time = d.toLocaleTimeString(navigator.language);
	// en attendant que {hour: '2-digit', minute:'2-digit'} fonctionne partout
	time = time.substring(0, time.length - 3);
	return time;
};

Handlebars.registerHelper("dayOrNight", function(sunrise, sunset, time){
	var period = 'night';
	if ( time > sunrise && time < sunset) {
		period = 'day';
	}
	return period;
});

Handlebars.registerHelper("epocToDate", function(seconds){
	var time = epocMsecToDate(seconds*1000);
	return time;
});

Handlebars.registerHelper("epocMsecToDate", function(milliseconds){
	var time = epocMsecToDate(milliseconds);
	return time;
});

Handlebars.registerHelper("msToKmh", function(ms){
	var kmh = ms * 3.6;
	return kmh.toFixed(0);
});

Handlebars.registerHelper("floatToInt", function(number){
	return number.toFixed(0);
});

Handlebars.registerHelper("windDirection", function(degrees){
	var symbols = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N"];
	degrees = (degrees + 11.25) / 22.5;
	var index = degrees.toFixed(0);
	return symbols[index];
});

Handlebars.registerHelper("cityNameForLink", function(string){
	var accent = [
		/[\340-\346]/g, // a
		/[\350-\353]/g, // e
		/[\354-\357]/g, // i
		/[\362-\370]/g, // o
		/[\371-\374]/g	// u
	];

	var noaccent = ['a','e','i','o','u'];

	string = string.toLowerCase();
	string = string.replace(/ /g, '-');

	for(var i = 0; i < accent.length; i++){
		string = string.replace(accent[i], noaccent[i]);
	}

	return string;
});

Handlebars.registerHelper("rainIcon", function(number){
	var icon;

	if ( number == 1 ) {
		icon = 'cloud';
	} else if ( number == 2 ) {
		icon = 'showers';
	} else if ( number == 3 ) {
		icon = 'rain-mix';
	} else if ( number == 4 ) {
		icon = 'rain';
	}

	return icon;
});

Handlebars.registerHelper("airQualityIndexColor", function(index){
	var color;

	if ( index <= 50 ) {
		color = "#009966";
	} else if ( index > 50 && index <= 100 ) {
		color = "#ffde33";
	} else if ( index > 100 && index <= 150 ) {
		color = "#ff9933";
	} else if ( index > 150 && index <= 200 ) {
		color = "#cc0033";
	} else if ( index > 200 && index <= 300 ) {
		color = "#660099";
	} else if ( index > 300 ) {
		color = "#7e0023";
	}

	return color;
});
